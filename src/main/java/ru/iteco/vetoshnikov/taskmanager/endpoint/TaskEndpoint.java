package ru.iteco.vetoshnikov.taskmanager.endpoint;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.iteco.vetoshnikov.taskmanager.api.ITaskEndpoint;
import ru.iteco.vetoshnikov.taskmanager.api.ITaskService;
import ru.iteco.vetoshnikov.taskmanager.dto.TaskDTO;

import java.util.List;

@Getter
@Setter
@Component
public class TaskEndpoint implements ITaskEndpoint {
    @Autowired
    ITaskService taskService;

    @Override
    public List<TaskDTO> listTasks(
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) return null;
        return taskService.allTasks(projectId);
    }

    @Override
    public void saveTask(
            @Nullable final TaskDTO taskDTO
    ) {
        if (taskDTO == null) return;
        taskService.add(taskDTO);
    }

    @Override
    public void deleteTask(
            @Nullable final String taskId
    ) {
        if (taskId == null) return;
        taskService.delete(taskId);
    }

    @Override
    public void deleteAllByProjectIdTask(
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) return;
        taskService.deleteAllByProjectId(projectId);
    }

    @Override
    public void editTask(
            @Nullable final TaskDTO taskDTO
    ) {
        if (taskDTO == null) return;
        taskService.edit(taskDTO);
    }

    @Override
    public TaskDTO getByProjectIdAndIdTask(
            @Nullable final String projectId,
            @Nullable final String id
    ) {
        if (projectId == null || projectId.isEmpty() || id == null || id.isEmpty()) return null;
        return taskService.getByProjectIdAndId(projectId, id);
    }

    @Override
    public TaskDTO getByIdTask(
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) return null;
        return taskService.getById(id);
    }
}
