package ru.iteco.vetoshnikov.taskmanager.api;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.dto.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @WebMethod
    List<ProjectDTO> listProjects();

    @WebMethod
    void saveProject(
            @WebParam(name = "projectDTO") @Nullable ProjectDTO projectDTO
    );

    @WebMethod
    void deleteProject(
            @WebParam(name = "projectDTO") @Nullable ProjectDTO projectDTO
    );

    @WebMethod
    void editProject(
            @WebParam(name = "projectDTO") @Nullable ProjectDTO projectDTO
    );

    @WebMethod
    ProjectDTO getByIdProject(
            @WebParam(name = "id") @Nullable String id
    );
}
