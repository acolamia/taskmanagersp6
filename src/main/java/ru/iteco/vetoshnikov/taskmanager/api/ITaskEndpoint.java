package ru.iteco.vetoshnikov.taskmanager.api;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {
    @WebMethod
    List<TaskDTO> listTasks(
            @WebParam(name = "projectId") @Nullable String projectId
    );

    @WebMethod
    void saveTask(
            @WebParam(name = "taskDTO") @Nullable TaskDTO taskDTO
    );

    @WebMethod
    void deleteTask(
            @WebParam(name = "taskId") @Nullable String taskId
    );

    @WebMethod
    void deleteAllByProjectIdTask(
            @WebParam(name = "projectId") @Nullable String projectId
    );

    @WebMethod
    void editTask(
            @WebParam(name = "taskDTO") @Nullable TaskDTO taskDTO
    );

    @WebMethod
    TaskDTO getByProjectIdAndIdTask(
            @WebParam(name = "projectId") @Nullable String projectId,
            @WebParam(name = "id") @Nullable String id
    );

    @WebMethod
    TaskDTO getByIdTask(
            @WebParam(name = "id") @Nullable String id
    );
}
